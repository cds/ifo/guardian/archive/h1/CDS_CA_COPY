from guardian import GuardState, GuardStateDecorator

# initial request on initialization
request = 'COPY'
# nominal operating state
nominal = 'COPY'

##############################

def get_chans():
    """Returns list of tuples from the txt file. This should
    keep the order so we can determine running order.
    """
    chans = []
    with open('/opt/rtcds/userapps/release/sys/h1/guardian/cds_ca_copy_list.txt', 'r') as f:
        for line in f.readlines():
            if line[0] == '#' or line == '\n':
                continue            
            l = line.rstrip().split(' ')
            chans.append(tuple(([chan[3:] for chan in l])))
    return chans

CHANNEL_LIST = get_chans()

WAIT_TIME = 0.1


####################
# STATES
####################

class INIT(GuardState):
    request = False
    def main(self):
        # turn down ezca write logging
        def logput(pv, value):
            message = "%s => %s" % (pv.pvname, value)
            ezca._log.debug(message)
        ezca._logput = logput
        return 'COPY'


class COPY(GuardState):
    index = 10
    request = True
    def main(self):
        self.channel_list = CHANNEL_LIST
        self.timer._logfunc = None
        self.timer['wait'] = WAIT_TIME

    def run(self):
        if not self.timer['wait']:
            return True
        for channel_pair in self.channel_list:
            read_from = ezca[channel_pair[1]]
            write_to = ezca[channel_pair[0]]
            if not read_from == write_to:
                ezca[channel_pair[0]] = read_from
            #ezca[channel_pair[0]] = ezca[channel_pair[1]]
        
        self.timer['wait'] = WAIT_TIME
        return True


##############################
edges = [
    ('INIT','COPY'),
    ]
